#

git pull origin
git pull gitlab

tic=$(date +%s)
gitdir=$(git rev-parse --git-dir)
qm=$(ipfs add -Q -n -w -r $gitdir/config . )
echo url: https://gateway.ipfs.io/ipfs/$qm
echo $tic: $qm >> qm.log
eval $(perl -S fullname.pl -a $qm | eyml)
echo name: $fullname
prev=$(git config ipfs.qm)
git config ipfs.qm $qm
git config ipfs.prev $prev

jekyll build
surge _site -d surge.plove.ml
path=$(pwd | sed -e "s,$HOME,,")
echo https://127.0.0.1:8088$path/_site/index.html
xdg-open http://127.0.0.1:8088$path/_site/index.html


git commit -a
git push origin
git push gitlab
